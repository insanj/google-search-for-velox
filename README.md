Google Search for Velox
=======================

Quick search from the Google app!

Wouldn't have been possible without Stopwatch and Carrox:
https://github.com/maxkatzmann/Velox-Stopwatch
https://github.com/hbang/Carrox
